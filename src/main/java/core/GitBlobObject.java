package core;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.nio.file.FileSystems;
import java.nio.file.Path;

import java.util.Arrays;
import java.util.zip.InflaterInputStream;

public class GitBlobObject {

    private final Path repo;
    private final String objectHash;
    private final Path objectDirectory;
    private final Path objectFile;


    public GitBlobObject(String path, String hash)
    {
        this.repo = FileSystems.getDefault().getPath(path);
        this.objectHash = hash;

        String objectDirectory = "objects/" + this.objectHash.substring(0, 2);
        String objectFile = this.objectHash.substring(2);

        this.objectDirectory = this.repo.resolve(objectDirectory);
        this.objectFile = this.objectDirectory.resolve(objectFile);
    }

    private String decompress() throws Exception {
        FileInputStream file = new FileInputStream(objectFile.toFile());

        ByteArrayInputStream byteInput = new ByteArrayInputStream(file.readAllBytes());
        InflaterInputStream inflaterInput = new InflaterInputStream(byteInput);

        StringBuilder builder = new StringBuilder();
        byte[] buffer = new byte[5];
        int read;
        while ((read = inflaterInput.read(buffer)) != -1)
            builder.append(new String(Arrays.copyOf(buffer, read)));

        return builder.toString();
    }

    public String getType() throws Exception
    {
        String content = this.decompress();
        return content.substring(0, content.indexOf(" "));
    }

    public String getContent() throws Exception{
        String content = this.decompress();
        return content.replaceAll("^blob [0-9]+\\x{00}", "");
    }
}
