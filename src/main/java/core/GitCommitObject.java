package core;

public class GitCommitObject {

    private final String hash;
    private final GitBlobObject object;

    public GitCommitObject(String repo, String hash)
    {
        this.hash = hash;
        this.object = new GitBlobObject(repo, hash);
    }

    public String getHash() throws Exception{
        return this.hash;
    }

    private String[] parse() throws Exception {
        return this.object.getContent()
                .replaceAll("^commit [0-9]+\\x{00}", "")
                .split("\n");
    }
    public String getTreeHash() throws Exception{
        return this.parse()[0]
                .replaceAll("^tree ", "");
    }

    public String getParentHash() throws Exception{
        return this.parse()[1]
                .replaceAll("^parent ", "");
    }

    public String getAuthor() throws Exception{
        return this.parse()[2]
                .replaceAll("author ", "")
                .replaceAll("[0-9 +]*$", "");
    }
}
