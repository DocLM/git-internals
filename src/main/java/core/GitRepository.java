package core;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;

public class GitRepository {

    private final Path repo;

    public GitRepository(String path)
    {
        this.repo = FileSystems.getDefault().getPath(path);
    }

    public String getHeadRef() throws IOException {
        Path head = this.repo.resolve("HEAD");
        BufferedReader fileReader = new BufferedReader(new FileReader(head.toFile()));

        return fileReader.readLine().substring(5);
    }

    public String getRefHash(String path) throws Exception {
        Path file = this.repo.resolve(path);
        BufferedReader fileReader = new BufferedReader(new FileReader(file.toFile()));

        return fileReader.readLine();
    }
}